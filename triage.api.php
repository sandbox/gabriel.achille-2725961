<?php
/**
 * Allows modules to alter the list of admin links for a step.
 *
 * @param &$admin_links
 *   An array of admin links, or FALSE if none were found before
 *   invoking this hook. Passed by reference.
 * @param $step
 *   The step.
 *
 * @see triage_render_step_admin_links()
 */
function hook_triage_step_admin_links_alter(&$admin_links, $step) {
  // Remove forbidden link (!)
  if (isset($admin_links['forbidden_link'])) {
    unset($admin_links['forbidden_link']);
  }
}
