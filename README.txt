CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

REQUIREMENTS
------------
This module requires the following modules:

 * JavaScript callback handler (https://www.drupal.org/project/js)


INTRODUCTION
------------

The Triage module allow to manage a Decision tree ...

 * Main module (triage): provide the backend services to send next step to a the client by JSON
 * Web front-end (triage_html): provide a HTML interface (page) to display the decision tree steps.
 (assuming you can actually use the core with non-web clients (App) without needed triage_html)
 * Admin interface (triage_admin): provide Drupal web interface to edit decision trees.