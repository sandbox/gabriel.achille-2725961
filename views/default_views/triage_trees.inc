<?php

// --- Paste exported view below ---

$view = new view();
$view->name = 'triage_trees';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'triage_tree';
$view->human_name = 'Administration: Triage trees';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Triage trees';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'administer triage tree';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'tid' => 'tid',
  'info' => 'info',
  'title' => 'title',
  'status' => 'status',
  'updated' => 'updated',
  'view_tree' => 'view_tree',
  'edit_tree' => 'view_tree',
  'delete_tree' => 'view_tree',
  'clone_tree' => 'view_tree',
  'check_tree' => 'view_tree',
);
$handler->display->display_options['style_options']['default'] = 'updated';
$handler->display->display_options['style_options']['info'] = array(
  'tid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'info' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'updated' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'view_tree' => array(
    'align' => '',
    'separator' => '&nbsp;',
    'empty_column' => 0,
  ),
  'edit_tree' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'delete_tree' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'clone_tree' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'check_tree' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Triage: Triage tree ID */
$handler->display->display_options['fields']['tid']['id'] = 'tid';
$handler->display->display_options['fields']['tid']['table'] = 'triage_tree';
$handler->display->display_options['fields']['tid']['field'] = 'tid';
$handler->display->display_options['fields']['tid']['label'] = 'ID';
$handler->display->display_options['fields']['tid']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['tid']['alter']['path'] = 'admin/content/triage/tree/[tid]';
/* Field: Triage: Admin title */
$handler->display->display_options['fields']['info']['id'] = 'info';
$handler->display->display_options['fields']['info']['table'] = 'triage_tree';
$handler->display->display_options['fields']['info']['field'] = 'info';
/* Field: Triage: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'triage_tree';
$handler->display->display_options['fields']['title']['field'] = 'title';
/* Field: Triage: Published */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'triage_tree';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Triage: Last updated date */
$handler->display->display_options['fields']['updated']['id'] = 'updated';
$handler->display->display_options['fields']['updated']['table'] = 'triage_tree';
$handler->display->display_options['fields']['updated']['field'] = 'updated';
$handler->display->display_options['fields']['updated']['date_format'] = 'short';
$handler->display->display_options['fields']['updated']['second_date_format'] = 'long';
/* Field: Triage: View link */
$handler->display->display_options['fields']['view_tree']['id'] = 'view_tree';
$handler->display->display_options['fields']['view_tree']['table'] = 'triage_tree';
$handler->display->display_options['fields']['view_tree']['field'] = 'view_tree';
$handler->display->display_options['fields']['view_tree']['label'] = 'Operations';
/* Field: Triage: Check link */
$handler->display->display_options['fields']['check_tree']['id'] = 'check_tree';
$handler->display->display_options['fields']['check_tree']['table'] = 'triage_tree';
$handler->display->display_options['fields']['check_tree']['field'] = 'check_tree';
$handler->display->display_options['fields']['check_tree']['element_label_colon'] = FALSE;
/* Field: Triage: Edit link */
$handler->display->display_options['fields']['edit_tree']['id'] = 'edit_tree';
$handler->display->display_options['fields']['edit_tree']['table'] = 'triage_tree';
$handler->display->display_options['fields']['edit_tree']['field'] = 'edit_tree';
$handler->display->display_options['fields']['edit_tree']['element_label_colon'] = FALSE;
/* Field: Triage: Delete link */
$handler->display->display_options['fields']['delete_tree']['id'] = 'delete_tree';
$handler->display->display_options['fields']['delete_tree']['table'] = 'triage_tree';
$handler->display->display_options['fields']['delete_tree']['field'] = 'delete_tree';
$handler->display->display_options['fields']['delete_tree']['element_label_colon'] = FALSE;
/* Field: Triage: Clone link */
$handler->display->display_options['fields']['clone_tree']['id'] = 'clone_tree';
$handler->display->display_options['fields']['clone_tree']['table'] = 'triage_tree';
$handler->display->display_options['fields']['clone_tree']['field'] = 'clone_tree';
$handler->display->display_options['fields']['clone_tree']['element_label_colon'] = FALSE;
/* Filter criterion: Triage: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'triage_tree';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['operator'] = 'contains';
$handler->display->display_options['filters']['title']['group'] = 1;
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
/* Filter criterion: Triage: Admin title */
$handler->display->display_options['filters']['info']['id'] = 'info';
$handler->display->display_options['filters']['info']['table'] = 'triage_tree';
$handler->display->display_options['filters']['info']['field'] = 'info';
$handler->display->display_options['filters']['info']['operator'] = 'contains';
$handler->display->display_options['filters']['info']['group'] = 1;
$handler->display->display_options['filters']['info']['exposed'] = TRUE;
$handler->display->display_options['filters']['info']['expose']['operator_id'] = 'info_op';
$handler->display->display_options['filters']['info']['expose']['label'] = 'Admin title';
$handler->display->display_options['filters']['info']['expose']['operator'] = 'info_op';
$handler->display->display_options['filters']['info']['expose']['identifier'] = 'info';
/* Filter criterion: Triage: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'triage_tree';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/content/triage/tree';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Triage trees';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
