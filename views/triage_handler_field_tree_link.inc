<?php

class triage_handler_field_tree_link extends views_handler_field {
  var $link_type;

  function construct() {
    // We need to set this property before calling the construct() chain
    // as we use it in the option_definintion() call.
    $this->link_type = $this->definition['link_type'];

    parent::construct();
    //$this->additional_fields['serial'] = 'serial';
  }

  function allow_advanced_render() {
    return FALSE;
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['label'] = array('default' => '', 'translatable' => TRUE);
    $options['text'] = array('default' => $this->link_type, 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );
  }

  function render($values) {
    $tid = $values->tid;

    $text = $this->options['text'];
    $path = drupal_get_path_alias($_GET['q']);
    switch ($this->link_type) {
      case 'view':
        $text = !empty($text) ? $text : t('view');
        if (module_exists('triage_html')) {
          $link = l($text, "triage/tree/$tid");
        }
        else {
          // default to the "check" view (but it shouldn't happen because
          // this views link field is defined in triage_html only (...)
          $link = l($text, "admin/content/triage/tree/$tid");
        }
        break;
      case 'check':
        $text = !empty($text) ? $text : t('check');
        $link = l($text, "admin/content/triage/tree/$tid", array('query' => array('destination' => $path)));
        break;
      case 'edit':
        $text = !empty($text) ? $text : t('edit');
        $link = l($text, "admin/content/triage/tree/$tid/edit", array('query' => array('destination' => $path)));
        break;
      case 'delete':
        $text = !empty($text) ? $text : t('delete');
        $link = l($text, "admin/content/triage/tree/$tid/delete", array('query' => array('destination' => $path)));
        break;
      case 'clone':
        $text = !empty($text) ? $text : t('clone');
        $link = l($text, "admin/content/triage/tree/$tid/clone", array('query' => array('destination' => $path)));
        break;
      default:
        return;
    }

    return $link;
  }
}