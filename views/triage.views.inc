<?php

/**
 * Implements hook_views_data().
 */
function triage_views_data() {
  // Expose triage tree table to views:

  $data['triage_tree']['table']['group'] = t('Triage');

  $data['triage_tree']['table']['base'] = array(
    'field' => 'tid',
    'title' => t('Triage tree'),
    'help' => t('Triage tree table contains de decision trees provided by the triage module'),
  );

  $data['triage_tree']['tid'] = array(
    'title' => t('Triage tree ID'),
    'help' => t('Uniq ID for the tree'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'title' => t('Serial number'),
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  $data['triage_tree']['title'] = array(
    'title' => t('Title'),
    'help' => t('Public title'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['triage_tree']['info'] = array(
    'title' => t('Admin title'),
    'help' => t('for admin purpose only'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['triage_tree']['description'] = array(
    'title' => t('Description'),
    'help' => t('Body text of the tree'),
    'field' => array(
      'handler' => 'views_handler_field_xss',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  $data['triage_tree']['description_final'] = array(
    'title' => t('Description final'),
    'help' => t('Free text to be display at the end of the tree.'),
    'field' => array(
      'handler' => 'views_handler_field_xss',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  $data['triage_tree']['status'] = array(
    'title' => t('Published'),
    'help' => t('0:no ; 1:yes'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
      'output formats' => array(
        'published-notpublished' => array(t('Published'), t('Not published')),
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Published'),
      'type' => 'yes-no',
      'use equal' => TRUE, // Use status = 1 instead of status <> 0 in WHERE statment
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['triage_tree']['created'] = array(
    'title' => t('Creation date'),
    'help' => t('Creation date'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'title' => t('Submitted'),
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );
  $data['triage_tree']['updated'] = array(
    'title' => t('Last updated date'),
    'help' => t('Last updated date'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'title' => t('Submitted'),
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );
 
  // admin links:
  $data['triage_tree']['check_tree'] = array(
    'title' => t('Check link'),
    'help' => t('Provide a simple link to check the tree (error msg, info...).'),
    'real field' => 'tid',
    'field' => array(
      'handler' => 'triage_handler_field_tree_link',
      'link_type' => 'check',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['triage_tree']['edit_tree'] = array(
    'title' => t('Edit link'),
    'help' => t('Provide a simple link to edit the tree.'),
    'real field' => 'tid',
    'field' => array(
      'handler' => 'triage_handler_field_tree_link',
      'link_type' => 'edit',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['triage_tree']['delete_tree'] = array(
    'title' => t('Delete link'),
    'help' => t('Provide a simple link to delete the tree.'),
    'real field' => 'tid',
    'field' => array(
      'handler' => 'triage_handler_field_tree_link',
      'link_type' => 'delete',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['triage_tree']['clone_tree'] = array(
    'title' => t('Clone link'),
    'help' => t('Provide a simple link to clone the tree.'),
    'real field' => 'tid',
    'field' => array(
      'handler' => 'triage_handler_field_tree_link',
      'link_type' => 'clone',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );




  return $data;
}
