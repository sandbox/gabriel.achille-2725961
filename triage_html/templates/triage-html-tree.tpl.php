<?php
/**
 * @file
 * Customize the display of a triage tree.
 *
 * Available variables:
 * - $intro_description: introduction content
 * - $tree_search: tree search form markup
 * - $accessible_triage: the HTML content of the steps so far
 * - $footer_classes: footer classes
 * - $footer_description: footer content
 */
?>
<div id="triage">
  <section class="intro">
    <div class="desc">
      <?php print $intro_description; ?>
    </div>
  </section>
  <?php if (!empty($tree_search)) : ?>
    <?php print $tree_search; ?>
  <?php endif; ?>
  <div class="accessible">
    <?php print $accessible_triage; ?>
  </div>
  <div class="fancy">
    <div class="inner"></div>

    <div class="<?php print $footer_classes; ?>" data-type="next_helper">
      <div class="inner">
        <?php print $footer_description; ?>
      </div>
      <div class="back">
        <a href="#" id="next-helper-back"><?php print t('Reset and return to top'); ?></a>
      </div>
    </div>
  </div>
</div>
