(function ($) {

  Drupal.behaviors.triage = {
    attach: function (context, settings) {
      $('#triage', context).once('triage', function() {
        //Drupal.triage.path = Drupal.settings.nswpTriageUI.path;
        Drupal.triage.path = Drupal.settings.triageHTML.path;
        //Drupal.triage.largeStepCharCount = Drupal.settings.nswpTriageUI.largeStepCharCount;

        // Disabling confirmation popup for now.
        //Drupal.triage.pathChangeConfirmationShown = true;

        //firstSteps = Drupal.settings.nswpTriageUI.firstSteps;
        firstSteps = Drupal.settings.triageHTML.firstSteps;

        if (!jQuery.isEmptyObject(firstSteps)) {

          // Instantiate section hierarchy.
          //Drupal.triage.sectionHierarchy = Drupal.settings.nswpTriageUI.sectionHierarchy;
          Drupal.triage.currentSectionHierarchy = Drupal.settings.triageHTML.sectionHierarchy;
          Drupal.triage.stepHierarchy = 0;
          Drupal.triage.loadedData = firstSteps;

          if (Drupal.triage.currentSectionHierarchy > 1) {
            Drupal.triage.showNextHelper(context);
          }

          // Drupal.triage.showLoadedStep(context);
          //var formattedStep = Drupal.triage.formatSteps(Drupal.triage.loadedData);
          //Drupal.triage.currentType = $(formattedStep).data('type');
          Drupal.triage.triggerBehaviours(context);
          Drupal.triage.loadedDataShown = true;

          // Attach dynamic event handler.
          // NSWP-469 Self-help tool: fix a js error that prevent ajax loading of next step
          // since upgrade of jquery lib...
          //$('a.triage-step').live('click', function(e) {
          $('#triage').on('click', 'a.triage-step', function(e) {
            if (Drupal.triage.loadingNextStep) {
              alert('The next step is still loading, please try again once it is done.');
            }
            else {
              Drupal.triage.lastType = $(this).data('type'); // TODO: since uniformisation of step this must not be relevant...
              Drupal.triage.lastHierarchy = $(this).data('hierarchy');
              var currentRef = $(this).data('ref');
              //currentRef += (currentRef == '') ? '' : ':';
              //var ref = currentRef + $(this).data('siid');
              var ref = currentRef + ':' + $(this).data('siserial');

              Drupal.triage.currentRef = ref;

              Drupal.triage.lastClick = $(this).attr('id');

              Drupal.triage.loadNextStep(ref, context);
            }
            e.preventDefault();
          });

          // Attach back button event handler.
          //$('a#next-helper-back').live('click', function(e) {
          $('div.back').on('click', 'a#next-helper-back', function(e) {
            // Reset hierarchy.
            Drupal.triage.currentSectionHierarchy = 1;
            var remainingSection = $('div.triage-section[data-hierarchy=1]');

            // Scroll to the first section.
            $('body, html').stop(false, true).animate({
              scrollTop: remainingSection.offset().top - 150
            }, 600, 'swing', function() {
              // Remove any 'clicked' classes.
              remainingSection.find('a.clicked').removeClass('clicked');

              // And remove all other sections.
              Drupal.triage.hideNextHelper();
              Drupal.triage.sectionRemove(context);
            });

            e.preventDefault();
          });

          Drupal.triage.attachDefaultBehaviours(context);
        }
      });
    }
  };

  // Instantiate the triage object.
  Drupal.triage = Drupal.triage || {};

  // Large/small step character differentiator.
  //Drupal.triage.largeStepCharCount = 50;

  // Loading semaphore.
  Drupal.triage.loadingNextStep = false;

  // Triage callback path.
  Drupal.triage.path = 'triage';

  // Last clicked section type.
  Drupal.triage.lastType = null;

  // Current section type.
  Drupal.triage.currentType = null;

  // Current ref.
  Drupal.triage.currentRef = null;

  // Current section behaviours.
  Drupal.triage.currentBehaviours = null;

  // Section hierarchy (depends on load order).
  Drupal.triage.currentSectionHierarchy = null;

  // Step hierarchy (depends on load order).
  //Drupal.triage.stepHierarchy = 0;
  Drupal.triage.stepHierarchy = 1;

  // Sections that need to be remove.
  Drupal.triage.sectionRemoveCount = null;

  // Last clicked link.
  Drupal.triage.lastClick = null;

  // Perform scrolling?
  Drupal.triage.noScroll = false;

  // Loaded step data.
  Drupal.triage.loadedData = null;

  // Is the loaded data shown?
  Drupal.triage.loadedDataShown = false;

  // Have we already shown the confirmation dialog at least once?
  //Drupal.triage.pathChangeConfirmationShown = false;

  Drupal.triage.loadNextStep = function(ref, context) {
    Drupal.triage.loadingNextStep = true;
    Drupal.triage.loadedDataShown = false;
    Drupal.triage.showLoader(context);

    $.ajax({
      //url: Drupal.triage.path + '/next/' + ref,
      url: Drupal.settings.basePath + 'js/triage/next-step/' + Drupal.settings.triageHTML.tid + '/' + ref,
      data: {js_module: 'triage', js_callback: 'next-step'},
      method: 'get',
      success: function(data) {
        Drupal.triage.loadingNextStep = false;
        Drupal.triage.hideLoader(context);

        // Get next hierarchy.
        //Drupal.triage.currentSectionHierarchy = $('.triage-section-' + Drupal.triage.lastType).data('hierarchy') + 1;
        Drupal.triage.currentSectionHierarchy = Drupal.triage.lastHierarchy;
        //Drupal.triage.stepHierarchy = 0;
        Drupal.triage.stepHierarchy = 1;

        if (!jQuery.isEmptyObject(data)) {
          Drupal.triage.loadedData = data;
          // Remove all lesser sections (if any).
          var waitingOnRemove = Drupal.triage.removeLesserSections(context);
          if (!waitingOnRemove) {
            Drupal.triage.showLoadedStep(context);
          }
        }
      }
    });
  }

  /**
   * Do fancy stuff and show new section.
   */
  Drupal.triage.showLoadedStep = function(context) {
    var formattedStep = Drupal.triage.formatSteps(Drupal.triage.loadedData);
    var backRefsExist = formattedStep.backRefsExist;
    formattedStep = formattedStep.out;

    if (Drupal.triage.currentSectionHierarchy > 0) {
      Drupal.triage.showNextHelper(context);
    }
    else {
      Drupal.triage.hideNextHelper(context);
    }

    // Remove any existing active classes.
    //if (Drupal.triage.currentType != null) {
    //  $('div.triage-section-' + Drupal.triage.currentType, context).removeClass('active');
    //}
    if (Drupal.triage.lastClick != null) {
      // Remove any existing 'clicked' classes in the same section,
      // then add the new one.
      $('a#' + Drupal.triage.lastClick).parents('ul.steps').find('a.clicked').each(function(event) {
        $(this).removeClass('clicked');
      });
      $('a#' + Drupal.triage.lastClick).addClass('clicked');
    }

    Drupal.triage.currentType = $(formattedStep).data('type');
    // $('section.triage-section-' + Drupal.triage.currentType, context).remove();
    $('#triage', context).find('.fancy > .inner').append(formattedStep);

    //$('div.triage-section-' + Drupal.triage.currentType, context).hide().animate({
    //$('div.triage-section[data-hierarchy=' + Drupal.triage.currentSectionHierarchy + ']', context).hide().animate({
    //  height: ['show', 'swing']
    //}, {
    //  duration: 200
    //}).addClass('active');
    $('div.triage-section[data-hierarchy=' + Drupal.triage.currentSectionHierarchy + ']', context).addClass('active');

    // Scroll to new section if not first load.
    if (Drupal.triage.lastClick != null && !Drupal.triage.noScroll) {
      $('body, html').stop(false, true).animate({
        //scrollTop: $('div.triage-section-' + Drupal.triage.currentType).offset().top - 80
        scrollTop: $('div.triage-section[data-hierarchy=' + Drupal.triage.currentSectionHierarchy + ']').offset().top - 80
      }, 600, 'swing');
    }
    else {
      Drupal.triage.noScroll = false;
    }


    // Trigger behaviours.
    Drupal.triage.triggerBehaviours(context);

    Drupal.triage.loadedDataShown = true;
  }

  Drupal.triage.triggerBehaviours = function(context) {
    if (!jQuery.isEmptyObject(Drupal.triage.currentBehaviours)) {
      $.each(Drupal.triage.currentBehaviours, function(index, behaviour) {
        //if (behaviour !== '') {
        if (behaviour.name !== '') {
          //var behaviourEvent = jQuery.Event(behaviour);
          var behaviourEvent = jQuery.Event(behaviour.name);
          //behaviourEvent.ref = Drupal.triage.currentRef;
          //behaviourEvent.currentType = Drupal.triage.currentType;
          //behaviourEvent.lastType = Drupal.triage.lastType;
          behaviourEvent.step = behaviour.step;

          $('div.triage-section-' + Drupal.triage.currentType, context).trigger(behaviourEvent);
        }
      });
    }
  }

  Drupal.triage.hideNextHelper = function(context) {
    $('.triage-section-next_helper', context).slideUp().fadeOut('slow');
  }

  Drupal.triage.showNextHelper = function(context) {
    $('.triage-section-next_helper', context).removeClass('next_helper-hidden').slideDown().fadeIn('slow');
  }

  Drupal.triage.hideLoader = function(context) {
    $('#triage-loader', context).stop(true, true).fadeOut(300, function() {
      $(this).remove();
    });
  }

  Drupal.triage.showLoader = function(context) {
    Drupal.triage.hideLoader();
    $('#triage', context).append(Drupal.triage.loader);
    $('#triage-loader', context).hide().fadeIn(600);
  }

  Drupal.triage.removeLesserSections = function(context) {
    // Used to notify the caller that we are waiting on a decision.
    var waiting = false;
    // If a section that needs to be removed was found.
    var found = false;

    // See if anything needs to be removed first.
    Drupal.triage.sectionRemoveCount = 0;
    $('#triage', context).find('div.triage-section').each(function(event) {
      var hierarchy = $(this).data('hierarchy');
      if (typeof hierarchy !== 'undefined' && hierarchy > Drupal.triage.currentSectionHierarchy) {
        found = true;
        Drupal.triage.sectionRemoveCount++;
        // return false;
      }
    });
    // Display a confirmation if required.
    if (found) {
      /*
      if (!Drupal.triage.pathChangeConfirmationShown) {
        waiting = true;
        $.confirm({
          'title'   : Drupal.t('Confirmation'),
          'message' : Drupal.t('This will reset your decision path to this point, are you sure you want to continue?'),
          'buttons' : {
            'Continue' : {
              'class' : 'success',
              'action': function() {
                Drupal.triage.pathChangeConfirmationShown = true;
                waiting = false;
                Drupal.triage.sectionRemove(context);
                Drupal.triage.showLoadedStep(context);
              }
            },
            'Cancel'  : {
              'class' : 'danger',
              'action': function() {
                Drupal.triage.pathChangeConfirmationShown = true;
              }
            }
          }
        });
      }
      else {
        waiting = true;
        Drupal.triage.sectionRemove(context);
      }
      */
      waiting = true;
      Drupal.triage.sectionRemove(context);
    }

    return waiting;
  }

  Drupal.triage.sectionRemove = function(context) {
    var sections = $('#triage', context).find('div.triage-section')
    var count = 1;
    sections.each(function(event) {
      var hierarchy = $(this).data('hierarchy');
      if (typeof hierarchy !== 'undefined' && hierarchy > Drupal.triage.currentSectionHierarchy) {
        $(this).slideUp(400, function() {
          $(this).remove();
          if (count == Drupal.triage.sectionRemoveCount) {
            Drupal.triage.showLoadedStep(context);
          }
          count++;
        });
      }
    });
  }

  Drupal.triage.backRefsExist = function(rawSteps) {
    var backRefsFound = false;

    $.each(rawSteps, function(key, step) {
      if (step.backrefs) {
        backRefsFound = true;
        return false;
      }
    });

    return backRefsFound;
  }

  /**
   * Format and return the raw step data as a nice string.
   */
  Drupal.triage.formatSteps = function(rawSteps) {
    var stepInfo = Drupal.triage.getStepInfo(rawSteps);
    var backRefsFound = false;

    //var stepsOut = {};
    var stepsOut = '';
    var substepType = '';
    var lastSubstepWeight = 0;
    //$.each(rawSteps, function(key, step) {
    $.each(rawSteps.substeps, function(key, step) {
      //if (typeof stepsOut[step.type] === 'undefined') {
      //  stepsOut[step.type] = '';
      //}
      // hierarchy of new step = current + 1
      var hierarchy = Drupal.triage.currentSectionHierarchy + 1;

      //var stepID = 'step-' + step.siid;
      var stepID = 'step-' + step.siserial;

      // Only create a link if back references to this step exist.
      var stepElementType = '';
      var stepElementExtra = '';
      //if (step.backrefs) {
      if (step.step_type == 'intermediate') {
        // The HTML5 draft spec allows the wrapping of block-level elements
        // with an 'a' tag (current as of 24/08/12).
        stepElementType = 'a';
        //stepElementExtra = ' href="' + Drupal.triage.path + '/' + Drupal.triage.currentRef + ":" + step.siid + '"';
        stepElementExtra = ' href="' + Drupal.triage.path + '?ref=' + step.ref + ":" + step.siserial + '"';
        backRefsFound = true;
      }
      else {
        stepElementType = 'div';
      }

      // Add in large/small step size indicator.
      //var stepSizeClass = (step.data.length > Drupal.triage.largeStepCharCount) ? ' large' : ' small';

      // Add (invisible) instructions for accessibility.
      var stepAccessibilityInfo = '<span class="element-invisible"> ' + Drupal.t('(Expand below)') + '</span>';

      //var stepOut = '<' + stepElementType + stepElementExtra + ' class="triage-step' + stepSizeClass + '" id="' + stepID + '" data-siid="' + step.siid + '" data-sid="' + step.sid + '" data-ref="' + step.ref + '" data-type="' + step.type + '">';
      var stepOut = '<' + stepElementType + stepElementExtra + ' class="triage-step' + '" id="' + stepID + '" data-siserial="' + step.siserial + '" data-ref="' + step.ref + '" data-type="' + step.step_type + '" data-hierarchy="' + hierarchy + '">';
      stepOut += step.step_data;
      if (stepElementType == 'a') {
        stepOut += stepAccessibilityInfo;
      }
      stepOut += '</' + stepElementType + '>';

      // Add admin links to edit/delete step:
      if (Drupal.settings.triageHTML.admin) {
        var template = Drupal.settings.triageHTML.adminTemplates.editLinks;
        var compiled_template = Handlebars.compile(template);
        var rendered = compiled_template({
          //edit_url: Drupal.settings.basePath + 'admin/content/triage/step-instance/' + step.siid + '/edit?destination=triage/tree/' + step.tid + encodeURIComponent('?ref=' + step.full_ref),
          edit_url: Drupal.settings.basePath + 'admin/content/triage/step-instance/' + step.siid + '/edit',
          //delete_url: Drupal.settings.basePath + 'admin/content/triage/step-instance/' + step.siid + '/delete?destination=triage/tree/' + step.tid + encodeURIComponent('?ref=' + step.full_ref)
          delete_url: Drupal.settings.basePath + 'admin/content/triage/step-instance/' + step.siid + '/delete'
        });
        stepOut += rendered;
      }

      //stepsOut[step.type] += Drupal.triage.stepWrapper(step.type, stepOut);
      stepsOut += Drupal.triage.stepWrapper(step.step_type, stepOut);

      // Assuming all the substep are the same type: we are saving the last one for later use:
      substepType = step.step_type;
      // keep a track of the last substep weight:
      lastSubstepWeight = step.weight;
    });

    // Add admin links to add new step there:
    if (Drupal.settings.triageHTML.admin) {
      // <li class="step step-intermediate odd"><a href="/nswp2/admin/content/triage/step-instance/add?tid=2&amp;piid=15&amp;weight=1" class="triage-step-add">Add new step here</a></li>
      var template = Drupal.settings.triageHTML.adminTemplates.addLink;
      var compiled_template = Handlebars.compile(template);
      var rendered = compiled_template({
        //add_url: Drupal.settings.basePath + 'admin/content/triage/step-instance/add?tid=' + rawSteps.step.tid + '&piid=' + rawSteps.step.siid + '&weight=45&destination=triage/tree/' + rawSteps.step.tid + encodeURIComponent('?ref=' + rawSteps.step.full_ref)
        add_url: Drupal.settings.basePath + 'admin/content/triage/step-instance/add?tid=' + rawSteps.step.tid + '&piid=' + rawSteps.step.siid + '&weight=' + (parseInt(lastSubstepWeight)+1)
      });
      stepsOut += Drupal.triage.stepWrapper(substepType, rendered);
    }

    var defaultStepInfo = Drupal.triage.defaultStepInfo;
    var stepTypesOut = '';
    Drupal.triage.currentBehaviours = [];
    //$.each(stepInfo, function(key, value) {
    //  // Merge default step info in.
    //  stepInfo[key] = $.extend({}, defaultStepInfo, value);
    //  stepTypesOut += Drupal.triage.formatStepType(stepInfo[key], stepsOut);

    //  // Keep a track of current behaviours.
    //  Drupal.triage.currentBehaviours.push(value.behaviour);
    //});
    stepTypesOut += Drupal.triage.formatStepType(stepInfo, stepsOut, substepType);
    // TODO: add Drupal.triage.currentBehaviours.push()???...
    // Check the first substeps for auto_continue behaviour:
    var first_substep = rawSteps.substeps[Object.keys(rawSteps.substeps)[0]];
    if (typeof first_substep !== 'undefined') {
      if (typeof first_substep.behaviour === 'string') {
        Drupal.triage.currentBehaviours.push({'name': first_substep.behaviour, 'step': first_substep});
      }
    }

    return {
      'out': stepTypesOut,
      'backRefsExist': backRefsFound
    };
  }

  Drupal.triage.getStepInfo = function(rawSteps) {
    //var stepInfo = rawSteps.info;
    //delete rawSteps.info;
    //return stepInfo;
    return rawSteps.step;
  }

  Drupal.triage.formatStepType = function(stepInfo, steps, substepType) {
    var stepTypeOut = '';

    //if (stepInfo.name != '') {
    if (stepInfo.subsection_title != '') {
      //var stepTypeHeader = '<h2>' + Drupal.t(stepInfo.name) + '</h2>';
      var stepTypeHeader = '<h2>' + stepInfo.subsection_title + '</h2>';
      stepTypeOut += stepTypeHeader;
    }

    //if (stepInfo.description != '') {
    if (stepInfo.subsection_description != '') {
      //var stepTypeDesc = '<p>' + Drupal.t(stepInfo.description) + '</p>';
      var stepTypeDesc = '<p>' + stepInfo.subsection_description + '</p>';
      stepTypeOut += stepTypeDesc;
    }

    //stepInfoOut = Drupal.triage.stepInfoWrapper(stepInfo.step_type, stepTypeOut);
    stepInfoOut = Drupal.triage.stepInfoWrapper(substepType, stepTypeOut);
    stepsOut = Drupal.triage.stepsWrapper(steps);
    //out = Drupal.triage.triageSectionWrapper(stepInfo.step_type, (stepInfoOut + stepsOut));
    out = Drupal.triage.triageSectionWrapper(substepType, (stepInfoOut + stepsOut));

    return out;
  }

  Drupal.triage.attachDefaultBehaviours = function(context) {
    //$('#triage', context).live('auto_continue', function(event) {
    $('div#content', context).on('auto_continue', '#triage', function(event) {
      // OLD:
      //var stepLink = $('.triage-section-' + event.currentType).find('a.triage-step');

      //var currentRef = event.ref;
      ////var ref = stepLink.data('siid');
      //var ref = stepLink.data('siserial');
      //ref = currentRef + ':' + ref;

      //Drupal.triage.currentRef = ref;
      //Drupal.triage.lastClick = stepLink.attr('id');
      //Drupal.triage.lastType = stepLink.data('type'); // todo: remove?
      //Drupal.triage.lastHierarchy = stepLink.data('hierarchy');
      //Drupal.triage.noScroll = true;

      //Drupal.triage.loadNextStep(ref, context);

      // NEW:
      // Here we are simulating the click on the step that triggered the auto_continue
      // ie see $('#triage').on('click', 'a.triage-step', function(e) {...})
      // get the stepLink (to have its hiearchy):
      var stepElement = $('#step-' + event.step.siserial);
      Drupal.triage.lastType = event.step.step_type;
      Drupal.triage.lastHierarchy = stepElement.data('hierarchy');
      var currentRef = event.step.ref;
      //currentRef += (currentRef == '') ? '' : ':';
      //var ref = currentRef + $(this).data('siid');
      var ref = currentRef + ':' + event.step.siserial;

      Drupal.triage.currentRef = ref;

      Drupal.triage.lastClick = 'step-' + event.step.siserial;

      Drupal.triage.loadNextStep(ref, context);
    });
  }

  Drupal.triage.stepWrapper = function(type, content) {
    var stripeClass = (Drupal.triage.stepHierarchy % 2 === 0) ? ' even' : ' odd';
    var step = '<li class="step step-' + type + stripeClass + '">';
    step += content;
    step += '</li>';

    Drupal.triage.stepHierarchy++;

    return step;
  }

  Drupal.triage.stepsWrapper = function(content) {
    var steps = '<ul class="steps">';
    steps += content;
    steps += '</ul>';

    return steps;
  }

  Drupal.triage.stepInfoWrapper = function(type, stepType) {
    var stepInfo = '<div class="step-type step-type-' + type + '">';
    stepInfo += stepType;
    stepInfo += '</div>';

    return stepInfo;
  }

  Drupal.triage.triageSectionWrapper = function(type, content) {
    var refType = ' data-type="' + type + '"';
    var hierarchy = (Drupal.triage.currentSectionHierarchy !== null) ? ' data-hierarchy="' + (Drupal.triage.currentSectionHierarchy + 1) + '"' : '';
    var stripeClass = (Drupal.triage.currentSectionHierarchy % 2 === 0) ? ' even' : ' odd';
    var triageSection = '<div class="triage-section triage-section-' + type + stripeClass + '"' + refType + hierarchy + '>';
    triageSection += content;
    triageSection += '</div>';
    return triageSection;
  }

  Drupal.triage.loader = function() {
    var loader = '<div id="triage-loader"><div class="inner">';

    loader += Drupal.t('Loading next step...');

    loader += '</div></div>';

    return loader;
  }

  Drupal.triage.defaultStepInfo = {
    step_type: '',
    subsection_title: '',
    subsection_description: ''
  };

})(jQuery);
