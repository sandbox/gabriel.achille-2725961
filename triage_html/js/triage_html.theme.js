(function ($) {

  Drupal.behaviors.triageTheme = {
    attach: function (context, settings) {
      $('div.triage-section-next_helper', context).once('triage-opacity-focus', function() {
        var section = $(this);
        $(this).find('input').focus(function() {
          section.addClass('active');
        }).blur(function() {
          section.removeClass('active');
        });
      });
    }
  };

})(jQuery);
