(function ($) {
    Drupal.behaviors.triageHtmlSearch = {
        attach: function (context, settings) {
            var stepsSearch = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                identify: function(obj) { return obj.siid; },
                remote: {
                    url: Drupal.settings.basePath + 'js/triage_search/suggestions/%TID/%QUERY?js_module=%MODULE&js_callback=%CALLBACK',
                    prepare: function (query, settings) {
                        // query: the search keywords
                        // settings: Object: {url: 'js/nswp_...', type: 'GET', dataType: 'json'}
                        var tid = $('#triage-html-search-steps-form input[name=tid]').val();
                        var module = $('#triage-html-search-steps-form input[name=keywords]').attr('data-js-module');
                        var callback = $('#triage-html-search-steps-form input[name=keywords]').attr('data-js-callback');
                        var textInput = $('#triage-html-search-steps-form input[name=keywords]');
                        settings.url = settings.url.replace('%TID', encodeURIComponent(tid));
                        settings.url = settings.url.replace('%QUERY', encodeURIComponent(query));
                        settings.url = settings.url.replace('%MODULE', encodeURIComponent(module));
                        settings.url = settings.url.replace('%CALLBACK', encodeURIComponent(callback));
                        return settings;
                    },
                    transform: function(response) {
                        //console.log(response);
                        return response.results;
                    },
                    rateLimitWait: 100, // default to 300ms. Set to 100ms for instant-like suggestions!
                },
            });
            var template_source =
                '<a href="{{url}}"><span class="header">{{{text}}}</span>';
                //+ '<span class="footer">{{{hierarchy}}}</span></a>';

            $('#edit-keywords', context).typeahead({
                    /*hint: true,*/
                    /*highlight: true,*/
                    minLength: 1
                },
                {
                    name: 'steps-search',
                    source: stepsSearch,
                    display: 'text_plain',
                    // to prevent bug : no result is displayed when the number of suggestions exactly equals the limit
                    // use infinity, as adviced here (https://github.com/twitter/typeahead.js/issues/1232#issuecomment-126193336)
                    limit: Infinity,
                    templates: {
                        empty: [
                            '<div class="empty-message">',
                            'No matches found. Please try to use the tree.',
                            '</div>'
                        ].join('\n'),
                        suggestion: Handlebars.compile(template_source)
                    }
                });
            // directly go to the content on suggestion-selection:
            $('#edit-keywords').bind('typeahead:select', function(ev, suggestion) {
                window.location.href = suggestion.url;
            });

            /*
            // override search link behavior: to submit the form:
            $('#edit-contract-search-button').click(function() {
                var keywords = $('#edit-keywords').val();
                if (keywords.length != 0 && keywords.slice(-1) != '*') {
                    keywords = keywords + '*';
                }
                document.location = Drupal.settings.basePath + "contracts-search?keywords=" + keywords;
                return false;
            });
            // on form submit: we adding * to the keywords to get the same results:
            $('#nswp-homepage-default-tiles-contracts-search-form').submit(function( event) {
                var keywords = $(this).find('input[name="keywords"]').val();
                if (keywords.length != 0 && keywords.slice(-1) != '*') {
                    $(this).find('input[name="keywords"]').val(keywords + '*');
                }
            });
            */
        }
    };
})(jQuery);
