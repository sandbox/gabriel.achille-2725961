<?php

/**
 * Implements hook_views_data_alter().
 */
function triage_html_views_data_alter(&$data) {
  if (isset($data['triage_tree'])) {
    // add view (html) link field in the view:
    $data['triage_tree']['view_tree'] = array(
      'title' => t('View link'),
      'help' => t('Provide a simple link to view the tree (HTML).'),
      'real field' => 'tid',
      'field' => array(
        'handler' => 'triage_handler_field_tree_link',
        'link_type' => 'view',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    );
  }
  
}
