(function ($, Drupal, window, document, undefined) {
    $(document).ready(function() {
        var DropDownButton = function (e) {
            var $this = $(this);
            var $parent  = $this.parent();
            $parent.toggleClass('open');
            return false;
        };
        var HideDropDownButton = function (e) {
            var $this = $(this);
            // NB: if this focusOut event was actually triggered by a click on one alternative submit:
            // do not hide the dropdown (because it prevent the button click to work)
            // instead just let the button submit the form
            if ($this.hasClass('open') && !submitDropdownMenuHovered) {
                $this.removeClass('open')
            }

        }
        $('.btn-dropdowns-split .btn-dropdown-toggle').on('click', DropDownButton);
        $('.btn-dropdowns-split').on('focusout', HideDropDownButton);

        var submitDropdownMenuHovered = false;
        $('#submit-dropdown-menu').hover(function () {
            // dropdownmenu is hovered:
            submitDropdownMenuHovered = true;
            //return;
        }, function () {
            // dropdownmenu is NOT hovered:
            submitDropdownMenuHovered = false;
        });

    });
})(jQuery, Drupal, this, this.document);

